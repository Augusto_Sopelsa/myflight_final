package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class GerenciadorPais implements Iterable<Pais>{
	private ArrayList<Pais> Paises;
	
	public GerenciadorPais(){
		Paises = new ArrayList<Pais>();
	}
	
	public void AdicionarPais(Pais p){
		Paises.add(p);
	}
	
	public ArrayList<Pais> ListarTodos(){
		return Paises;
	}
	
	public void CarregaDados() throws IOException{
		Path path2 = Paths.get("countries.dat");
		try(Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]");
			String header = sc.nextLine();
			String cod, nome;
			while (sc.hasNext()) {
				cod = sc.next();
				nome = sc.next();
				//System.out.format("%s - %s%n", cod, nome);
				Paises.add(new Pais(cod, nome));
			}
		}
		catch(IOException x){
			System.err.format("N�o foi possivel ler o arquivo", x);
		}
	}
	
	@Override
	public String toString(){
		StringBuilder aux = new StringBuilder();
		for(Pais p: Paises)
			aux.append(p.toString()+"\n");
		return aux.toString();
	}
	
	@Override
	public Iterator<Pais> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<Pais>() {

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Pais next() {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}
}