package pucrs.myflight.modelo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class GerenciadorCias implements Iterable<CiaAerea> {
	
	private ArrayList<CiaAerea> empresas;
	private HashMap<String, CiaAerea> companiesMap;

	public GerenciadorCias() {
		empresas = new ArrayList<>();
		companiesMap = new HashMap<>();
	}

	public void adicionar(CiaAerea cia) {
		empresas.add(cia);
		companiesMap.put(cia.getCodigo(), cia);
	}
	
	public void gravaSerial() throws IOException {
		Path arq1 = Paths.get("cias.ser");
		try (ObjectOutputStream oarq = new ObjectOutputStream(Files.newOutputStream(arq1))) {
		  oarq.writeObject(empresas);
		}
		catch(IOException e) {
		  System.out.println(e.getMessage());
		  System.exit(1);
		}
	}
	
	public void carregaSerial() throws IOException {
		Path arq1 = Paths.get("cias.ser");
		try (ObjectInputStream iarq = new ObjectInputStream(Files.newInputStream(arq1))) {
		  empresas = ((ArrayList<CiaAerea>) iarq.readObject());
		}
		catch(ClassNotFoundException e) {
		  System.out.println("Classe CiaAerea não encontrada!");
		  System.exit(1);
		}
	}

	public void carregaDados() throws IOException {
		Path path1 = Paths.get("airlines.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabeçalho
			String id, nome;
			while (sc.hasNext()) {
				id = sc.next();
				nome = sc.next();
				//System.out.format("%s - %s%n", id, nome);
				CiaAerea cia = new CiaAerea(id, nome);
				empresas.add(cia);
				companiesMap.put(id, cia);
			}
		}
	}

	public ArrayList<CiaAerea> listarTodas() {
		return new ArrayList<CiaAerea>(empresas);
	}

	public CiaAerea buscarCodigo(String codigo) {
		return companiesMap.get(codigo);
		/*for (CiaAerea c : empresas) {
			if (codigo.equals(c.getCodigo()))
				return c;
		}
		return null; // não achamos!*/
	}
	
	public boolean containsCompany(String code) {
		return companiesMap.containsKey(code);
	}

	public CiaAerea buscarNome(String nome) {
		for (CiaAerea c : empresas) {
			if (nome.equals(c.getNome()))
				return c;
		}
		return null; // não achamos!
	}

	@Override
	public Iterator<CiaAerea> iterator() {
		return  new Iterator<CiaAerea>(){
			private int posicaoCorrente=0;
			
			@Override
			public boolean hasNext() {
				return posicaoCorrente < empresas.size();
			}

			@Override
			public CiaAerea next() {
				return empresas.get(posicaoCorrente++);
				
			}
		};
	}
}
