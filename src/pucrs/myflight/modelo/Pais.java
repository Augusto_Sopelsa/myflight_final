package pucrs.myflight.modelo;


public class Pais{
	private String codPais;
	private String NomePais;
	
	public Pais(String cod, String nome){
		this.codPais = cod;
		this.NomePais = nome;
	}
	
	public String getCodPais(){
		return codPais;
	}
	
	public String getNomePais(){
		return NomePais;
	}
	
	@Override
	public String toString(){
		return codPais + "-" + NomePais;
	}
}