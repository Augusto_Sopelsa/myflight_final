package pucrs.myflight.modelo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class RoutesGraph {
	
	class Node {
	    String label;
	    Set<Aeroporto> adjacencyList;
	    public Node (String label, Set<Aeroporto> adjacencyList) {
			this.label = label;
			this.adjacencyList = adjacencyList;
		}
	    public String toString() {
			String s = "";
			s += label + " --> ";
			for (Aeroporto ap : adjacencyList) {
				s += ap.getCodigo()+" ";
			}
			return s+"\n";
		}
	}
	
	HashMap<String, Node> graph = new HashMap<String, Node>();
	
	public RoutesGraph(GerenciadorRotas gerRotas) {
		for (Rota r : gerRotas) {
			String airportCode = r.getOrigem().getCodigo();
			if (graph.containsKey(airportCode)) {
				graph.get(airportCode).adjacencyList.add(r.getDestino());
			} else {
				Set<Aeroporto> hs = new HashSet<>();
				hs.add(r.getDestino());
				graph.put(airportCode, new Node(airportCode, hs));
			}
		}
	}
	
	public LinkedList<LinkedList<String>> routesWithinGivenTime(int minutes, Aeroporto airport) {
		LinkedList<LinkedList<String>> listOfListOfRoutes = new LinkedList<>();
		double kilometersPerMinute = ((double)885)/((double)60);
		double maxDistance = kilometersPerMinute*minutes;
		for (Aeroporto ap : get(airport).adjacencyList) {
			if (ap.getLocal().distancia(airport.getLocal()) < maxDistance) {
				LinkedList<String> ll = new LinkedList<>(Arrays.asList(new String[] {airport.getCodigo(), ap.getCodigo()}));
				listOfListOfRoutes.add(ll);
			}
		}
		return listOfListOfRoutes;
	}
	
	public Set<ArrayList<String>> allRoutesBetweenTwoAirports(Aeroporto origin, Aeroporto destination) {
		Set<ArrayList<String>> setOfLists = new HashSet<>();
		
		Set<Aeroporto> adjacentToDestination = new HashSet<>();
		for (Aeroporto ap : get(destination).adjacencyList) {
			adjacentToDestination.add(ap);
		}
		
		Set<Aeroporto> adjacentToOrigin = new HashSet<>();
		for (Aeroporto ap : get(origin).adjacencyList) {
			adjacentToOrigin.add(ap);
		}
		
		if (adjacentToOrigin.contains(destination)) {
			ArrayList<String> al = new ArrayList<>(Arrays.asList(new String[] {origin.getCodigo(), destination.getCodigo()}));
			setOfLists.add(al);
		}
		
		adjacentToOrigin.remove(destination);
		adjacentToDestination.remove(origin);
		
		for (Aeroporto apAdjToOrig : adjacentToOrigin) {
			
			if (get(apAdjToOrig).adjacencyList.contains(destination)) {
				ArrayList<String> al = new ArrayList<>(Arrays.asList(
						new String[] {origin.getCodigo(), apAdjToOrig.getCodigo(), destination.getCodigo()}));
				setOfLists.add(al);
			}
			
			for (Aeroporto apAdjToDest : adjacentToDestination) {
				if ( get(apAdjToDest).adjacencyList.contains(apAdjToOrig) ) {
					ArrayList<String> al = new ArrayList<>(Arrays.asList(
							new String[] {origin.getCodigo(), apAdjToOrig.getCodigo(), apAdjToDest.getCodigo(), destination.getCodigo()}));
					setOfLists.add(al);
				}
			}
		}
		return setOfLists;
	}
	
	public Node get(Aeroporto ap) {
		return graph.get(ap.getCodigo());
	}
	
	/*
	 * Returns a list of "valid" airports
	 */
	public Set<String> getAirports() {
		TreeSet<String> aba = new TreeSet<>();
    	aba.addAll(graph.keySet());
		return aba;
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (Map.Entry<String, Node> entry : graph.entrySet()) {
			s.append(entry.getValue());
		}
		return s.toString();
	}
}
